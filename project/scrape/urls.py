from django.conf.urls import url
from django.urls import path, include
from . import views
from rest_framework import routers
from .views import PageView

router = routers.DefaultRouter()
router.register(r'stats', views.StatsView, basename="stats")
router.register('^author/(?P<author>.+)/$', PageView ,basename="author")
router.register(r'authors',views.AuthorView, basename="authors")


urlpatterns = [

    path("", include(router.urls))
]