
from collections import Counter
from rest_framework import viewsets
from .models import Page
from .serializers import authorserializer, statsserializer, postserializer
from django.shortcuts import get_object_or_404


# Create your views here.

class AuthorView(viewsets.ModelViewSet):
    serializer_class = authorserializer
    # queryset= Page.objects.all()

    def get_queryset(self):
        queryset = Page.objects.distinct('author')
        return queryset

class PageView(viewsets.ModelViewSet):
    serializer_class = postserializer
    def get_queryset(self,request,*args,**kwargs,):
        queryset = Page.objects.all()
        return Page.objects.filter(event=self.kwargs['author'])

class StatsView(viewsets.ModelViewSet):
    serializer_class = statsserializer
    queryset = Page.objects.all()









