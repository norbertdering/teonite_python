from rest_framework import serializers
from .models import Page
from collections import Counter

class statsserializer(serializers.ModelSerializer):

    counted_words = serializers.SerializerMethodField()
    class Meta:
        model = Page
        fields = ["author","counted_words"]

    def get_counted_words(self,obj):
        query = Counter(obj.post.split()).most_common(10)
        return  query


class postserializer(serializers.ModelSerializer):

    counted_words = serializers.SerializerMethodField()
    class Meta:
        model = Page
        fields = ["counted_words","author"]

    def get_counted_words(self,obj):
        query = Counter(obj.post.split()).most_common(10)
        return  query


class authorserializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = ["author"]






